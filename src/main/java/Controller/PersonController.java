package Controller;

import Model.Person;
import Service.Person.PersonService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

import java.io.IOException;
import java.util.List;

public class PersonController {

    PersonService personService;

    public PersonController() {
        personService = new PersonService();
    }


    public void getAllPerson(Context ctx) throws IOException {
        List<Person> result = personService.getAll();
        if (result.size() == 0) {
            throw new NotFoundResponse("No person is found in the database");
        }

        ctx.json(result);
    }

    public void getPersonByName(Context ctx) throws IOException {
        String name = ctx.pathParam("name");
        List<Person> result = personService.getByName(name);

        if (result.size() == 0) {
            throw new NotFoundResponse("No person is found in the database");
        }

        ctx.json(result);
    }

    public void createPerson(Context ctx) throws IOException {
        Person person = ctx.bodyAsClass(Person.class);
        String id = personService.createPerson(person);

        ctx.json(id);
    }

}
