package Service.Person;

import Model.Person;
import Service.ElasticClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.CreateResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersonService {
    ElasticsearchClient client;

    public PersonService() {
        client = ElasticClient.getInstance();
    }

    public List<Person> getAll() throws IOException {
        SearchResponse<Person> search = client.search(s -> s.index("person").query(q -> q.matchAll(m -> m.queryName("allPerson"))), Person.class);
        List<Person> persons = new ArrayList<>();
        for (Hit<Person> hit : search.hits().hits()) {
            persons.add(hit.source());
        }
        return persons;
    }

    public List<Person> getByName(String name) throws IOException {
        SearchResponse<Person> search = client.search(s -> s.index("person").query(q -> q.term(t -> t.field("firstName").value(name))), Person.class);

        List<Person> persons = new ArrayList<>();
        for (Hit<Person> hit : search.hits().hits()) {
            persons.add(hit.source());
        }
        return persons;
    }

    public String createPerson(Person person) throws IOException {
        CreateResponse createResponse = client.create(c -> c.index("person").document(person));
        return createResponse.id();
    }


}
