import Controller.PersonController;
import Service.ElasticClient;
import io.javalin.Javalin;

public class Application {
    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7070);

        ElasticClient.getInstance();
        PersonController personController = new PersonController();

        app.get("/person", personController::getAllPerson);
        app.get("/person/{name}", personController::getPersonByName);
        app.post("/person", personController::createPerson);


    }
}
